from flask import Flask , request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

app = Flask(__name__)

# db configuration
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:<db_password>@localhost/<db_name>'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# initialize db
db = SQLAlchemy(app)
mar = Marshmallow(app)

class Contact_details(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True)
    email = db.Column(db.String(50), unique=True)
    phone = db.Column(db.Integer)

    def __init__(self, name, email, phone):
        self.name = name
        self.email = email
        self.phone = phone


class Contact_detailsSchema(mar.Schema):
    class Meta:
        fields = ('id', 'name', 'email', 'phone')


# init schema
contact_details_schema = Contact_detailsSchema()
contact_details_schema = Contact_detailsSchema(many=True)

#creating a contact
@app.route('/contact', methods=['POST'])
def add_contact():
    name = request.json['name']
    email = request.json['email']
    phone = request.json['phone']

    new_contact = Contact_details(name, email, phone)

    db.session.add(new_contact)
    db.session.commit()

    return contact_details_schema.jsonify(contact_details_schema)


# get all contacts
@app.route('/contacts', methods=['GET'])
def get_contacts():
    all_contacts = Contact_details.query.all()
    result = contact_details_schema.dump(all_contacts)
    return jsonify(result)


# to return a single contact detail of id
@app.route('/contact/<id>', methods=['GET'])
def get_contact(id):
    contact = Contact_details.query.get(id)
    return contact_details_schema.jsonify(contact)


# update a contact
@app.route('/contact/<id>', methods=['PUT'])
def update_contact(id):
    contact = Contact_details.query.get(id)

    name = request.json['name']
    email = request.json['email']
    phone = request.json['phone']

    contact.name = name
    contact.email = email
    contact.phone = phone

    db.session.commit()

    return contact_details_schema.jsonify(contact)


# delete a contact
@app.route('/contact/<id>', methods=['DELETE'])
def delete_contact(id):
    contact = Contact_details.query.get(id)
    db.session.delete(contact)
    db.session.commit()
    return contact_details_schema.jsonify(contact)

db.create_all()
#Running app
if __name__ == '__main__':
    app.run(debug=True)
