here, i am running app on port 5000.

https://localhost:5000/contacts/, returns all contacts
https://localhost:5000/contact/id, method: "get" returns a specific contact
https://localhost:5000/contact, method: "post" post a specific contact in the database
https://localhost:5000/contact/id, method: "put" updates a specific contact
https://localhost:5000/contact/id, method: "delete" deletes a specific contact

